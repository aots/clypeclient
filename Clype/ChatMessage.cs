﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Clype
{
    public class ChatMessage: INotifyPropertyChanged
    {
        private Brush _brush;
        public Brush Brush { get { return _brush; }
            set
            {
                if(_brush.Equals(value))
                    return;
                _brush = value;
                OnPropertyChanged();
            }
        }
        public string Name
        {
            get;set;
        }

        public string Message
        {
            get;set;
        }

        public string Time
        {
            get;set;
        }

        public ChatMessage(string name, string message, string time, Brush brush)
        {
            Name = name;
            Message = message;
            Time = time;
            _brush = brush;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
