﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Clype
{
    public class ContactsDictionary: Dictionary<string, string>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void _onProperyChanged([CallerMemberName]string callerName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(callerName));
        }

        public new void Add(string key, string value)
        {
            base.Add(key, value);
            _onProperyChanged();
        }

        public void Save() {
            throw new NotImplementedException();
        }
        public void Load()
        {
            throw new NotImplementedException();
        }
    }
}
