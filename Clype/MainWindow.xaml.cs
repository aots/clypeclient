﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Prism.Commands;
using System.Media;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Hardcodet.Wpf.TaskbarNotification;
using System.Collections.Generic;

namespace Clype
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow: INotifyPropertyChanged
    {
        private static SoundPlayer PowerupSound = new SoundPlayer(Properties.Resources.Haha);
        private static SoundPlayer ExitSound = new SoundPlayer(Properties.Resources.Goodbye);
        private static Brush BackColorBrush = new SolidColorBrush(Color.FromArgb(0x22, 0x41, 0xb1, 0xe1));
        private static Thread _thread = new Thread(ThreadStart);
        private static IPEndPoint _remoteEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 14882);
        private static void ThreadStart(object o)
        {
            while (true)
            {
                var recvData = _udp.Receive(ref _remoteEP);
                var type = Encoding.UTF8.GetString(recvData, 0, 3);
                if (type == "mes")
                {
                    var message = Encoding.UTF8.GetString(recvData, 3, recvData.Length -3 - 19);
                    var time = Encoding.UTF8.GetString(recvData, recvData.Length - 19, 19);
                    _onMessageReceived(new ChatMessage(_remoteEP.Address.ToString(), message, time, BackColorBrush));
                    var bytes = Encoding.UTF8.GetBytes("rec" + "mes" + message + time);
                    _udp.Send(bytes, bytes.Length, new IPEndPoint(_remoteEP.Address, 14882));
                }
                else if (type == "rec")
                {
                    var message = Encoding.UTF8.GetString(recvData, 6, recvData.Length -6 -19);
                    var time = Encoding.UTF8.GetString(recvData, recvData.Length - 19, 19);
                    _onConfirmationReceived(new ChatMessage("", message, time, BackColorBrush));
                }
            }
        }

        private static ContactsDictionary SavedDictionary { get; set; }
        private static EventHandler<ChatMessage> OnMessageReceived;
        private static void _onMessageReceived(ChatMessage chatMessage)
        {
            OnMessageReceived?.Invoke(null, chatMessage);
        }

        private static EventHandler<ChatMessage> OnConfirmationReceived;

        private static void _onConfirmationReceived(ChatMessage chatMessage)
        {
            OnConfirmationReceived?.Invoke(null, chatMessage);
        }

        private static UdpClient _udp = new UdpClient(14882);
        private WindowState _prevWindowState = WindowState.Normal;
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _receiver;

        public string Receiver
        {
            get
            {
                return _receiver;
            }
            set
            {
                if (_receiver == value)
                    return;
                _receiver = value;
                OnPropertyChanged();
            }
        }

        private string _messageText;
        public string MessageText
        {
            get { return _messageText; }
            set
            {
                if (_messageText == value)
                 return;
                _messageText = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Contact> SavedCollection
        {
            get;set;
        }

        public ObservableCollection<ChatMessage> ChatMessageList
        {
            get;
            set;
        }

        private int? _selectedContactIndex;
        public int? SelectedContactIndex {
            get{
                return _selectedContactIndex;
            }
            set{
                if (_selectedContactIndex == value)
                    return;
                _selectedContactIndex = value;
                OnPropertyChanged();
            }
        }
        public DelegateCommand UnMinimize { get; set; }
        public DelegateCommand ExitApplication { get; set; }
        public DelegateCommand<string> WriteToCommand { get; set; }
        public DelegateCommand<string> SaveCommand { get; set; }
        public DelegateCommand<string> BlockCommand { get; set; }

        private static Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;

        public MainWindow()
        { 
            Closed += MainWindow_Closed;
            DataContext = this;
            Receiver = "127.0.0.1";
            _thread.IsBackground = true;
            _thread.Start();
            ChatMessageList = new ObservableCollection<ChatMessage>();
            SavedCollection = new ObservableCollection<Contact>();
            SavedDictionary = new ContactsDictionary();
            SavedDictionary.PropertyChanged += (s, e) =>
            {
                OnPropertyChanged("SavedDictionary");
            };
            OnMessageReceived += (sender, message) =>
            {
                Dispatcher.Invoke(()=> {
                    PowerupSound.Play();
                    ChatMessageList.Add(message);
                    if(WindowState == WindowState.Minimized || !IsVisible)
                        TaskBar.ShowBalloonTip("Clype messenger", "Пришло новое сообщение", BalloonIcon.Info);
                });
            };
            OnConfirmationReceived += (sender, message) =>
            {
                Dispatcher.Invoke(() =>
                {
                    for (int i = ChatMessageList.Count - 1; i >= 0; i--)
                    {
                        if (ChatMessageList[i].Message == message.Message && ChatMessageList[i].Time == message.Time &&
                            ChatMessageList[i].Brush.Equals(Brushes.Tomato))
                        {
                            ChatMessageList[i].Brush = Brushes.GhostWhite;
                            break;
                        }
                    }
                });
            };
            UnMinimize = new DelegateCommand(() => { Show();
                                                       WindowState = _prevWindowState;
                                                       Activate();
                                                       Topmost = true;
                                                       Topmost = false;
                                                       Focus();
            });
            WriteToCommand = new DelegateCommand<string>((str) =>
            {
                Receiver = str;
            });
            SaveCommand = new DelegateCommand<string>((str =>
            {
                SavedCollection.Add(new Contact(str,str));
                SelectedContactIndex = SavedCollection.Count-1;
            }));
            ExitApplication = new DelegateCommand(() =>
            {
                ExitSound.PlaySync();
                Application.Current.Shutdown(0);
            });
            InitializeComponent();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            TaskBar = null;
        }

        private void _sendMessage()
        {
            if (!string.IsNullOrEmpty(MessageText))
            {
                var dt = DateTime.Now.ToString("hh:mm:ss dd.MM.yyyy");
                ChatMessageList.Add(new ChatMessage("Вы:", MessageText, dt, Brushes.Tomato));
                MessageScrollViewer.ScrollToEnd();
                var message = "mes" + MessageText + dt;
                var messageBytes = Encoding.UTF8.GetBytes(message);
                _udp.Send(messageBytes, messageBytes.Length, new IPEndPoint(Dns.GetHostAddresses(Receiver)[0], 14882));
                MessageText = "";
            }
        }

        private void SendMessageButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _sendMessage();
        }

        private void TextBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                _sendMessage();
                e.Handled = true;
            }
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
        private void MainWindow_OnStateChanged(object sender, EventArgs e)
        {
            _prevWindowState = WindowState;
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                WindowState = _prevWindowState;
            }
        }

        private void TaskbarIcon_OnTrayRightMouseDown(object sender, RoutedEventArgs e)
        {
            TaskBar.ContextMenu.IsOpen = true;
            //TaskBar.ShowCustomBalloon(new Menu(),PopupAnimation.Fade, 10000);
        }

        private void Address_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var tb = (TextBox)sender;
            Receiver = tb.Tag.ToString();
        }
    }
}
