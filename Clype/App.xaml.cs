﻿using System;
using System.Windows;

namespace Clype
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            MessageBox.Show("Непредвиденная ошибка:\r\n" + unhandledExceptionEventArgs.ExceptionObject + "\r\nПрограмма будет закрыта.",
                "ОШИБКА СТОП 000000000000", MessageBoxButton.OK, MessageBoxImage.Error);
            MainWindow.Close();
            Shutdown(-1);
        }
    }
}
